# Smash ⨳

Outline # – v2.

## Initialisation

Les dépendances `scss` sont gérées avec [Bower (1.3.12)](https://github.com/bower/bower), il faut donc commencer par les installer avec :

`$ bower install`

[Sass](https://github.com/sass/sass), [Autoprefixer](https://github.com/postcss/autoprefixer) et [Compass](https://github.com/Compass/compass) (ce dernier étant optionnel) sont pris en charge par [Bundler (1.7.6)](https://github.com/bundler/bundler/) :

`$ bundle install`

[Styledocco](https://github.com/jacobrask/styledocco) et [Sass Globbing](https://github.com/chriseppstein/sass-globbing) sont prit en charge par [npm (1.4.3)](https://github.com/npm/npm) :

`$ npm install`

La compilation est ensuite lancée via :

`$ bundle exec compass compile`

ou la surveillance via :

`$ bundle exec compass watch`

La documentation est générée avec :

`$ styledocco --include css/screen.css scss/`
    
(Et il sera possible à terme de passer également via Grunt pour la compilation et la documentation.)

Côté HTML, voici un exemple de code pour appeler les feuilles de styles :

    <!--[if lte IE 8]>
    <link rel="stylesheet" media="screen" href="css/screen-ie8.css" />
    <![endif]-->
    <!--[if gt IE 8]><!-->
    <link rel="stylesheet" media="screen" href="css/screen.css" />
    <!--<![endif]-->
    <link rel="stylesheet" media="print"  href="css/print.css" />

## Objectifs

Cette sélection de _patterns_ CSS est forcement subjective. Toute proposition de contribution est donc, bien sûr, la bienvenue.

On peut néanmoins lister quelques objectifs et pré-requis :

 - la concision ;
 - la pertinence ;
 - l’idée qu’il s’agit là d’une base de travail.

### Concision

L’un des objectifs était de laisser la base de code suffisamment courte pour qu’elle reste assimilable et dont le poids serait négligeable.
Actuellement la bibliothèque une fois optimisée fait moins de 10 Ko.

Idéalement une base de code CSS ne devrait générer aucun poids quand elle n’est pas utilisée. C’est chose possible notamment avec les `mixins`, qui ne génèrent du code que s’ils sont appelés.

Cependant lorsque les `mixins` ne retournent que très peu de code, on perd cet intérêt.

Je prends par exemple : `@mixin smash-align-right { text-align: right }`.

Si l’on souhaite adopter la philosophie BEM (c’est notre cas), il sera utile de passer via une classe HTML pour utiliser le `mixin`.

Il faudrait donc écrire : `.smash-align-right { @include smash-align-right }`.

Répété de nombreuses fois et on voit bien que le travail de mutualisation est raté.
Chaque projet initié devrait être complété par de multiples déclarations de ce type : aucun gain en terme de ré-utilisabilité du code.

L’alternative pourrait être l’`extend` ou le `placeholder`, mais ces mécanismes sont très peu lisibles après coup (du fait de la factorisation à outrance des sélecteurs).
Ils nécessitent également des contextes d’utilisations très particuliers, qui les font sortir du _scope_ de cette bibliothèque.

Suite à ces constats, il semble plus intéressant d’écrire directement des classes : `.smash-align-right { text-align: right }`.

Certes elles ne seront pas toujours utilisées, et la CSS générée sera un peu plus lourde mais :

 - on s’évite ainsi les problèmes évoqués ;
 - le poids ajouté reste toutefois relatif ;
 - il reste toujours possible (sur les projets qui demandent une forte optimisation des CSS) de commenter les classes qui ne seront pas utilisées.

Les `mixins` n’en restent pas moins dénués d’intérêt. Quand ils embarquent de nombreuses propriétés ils permettent en effet d’améliorer la lisibilité du code.
Leur utilisation sera complétée d’une déclaration de classe pour qu’il soit alors possible d’appeler les propriétés soit via CSS (`@include`), soit via HTML (classe).

### Pertinence

Second objectif : se limiter à des classes qui sont identifiées comme très génériques ou comme structurantes sur la présentation.

Le masquage visuels d’éléments qui doivent restent accessibles aux machines (`.smash-visually-hidden`) est un bon exemple de composant générique, fréquemment utilisé de projet en projet. 

D’autres composants, plus courts, sont intéressant car ils structurent fortement la mise en page : `.smash-inline-block` ou `.smash-float-left` par exemple.

Mais des composants trop spécifiques ne doivent pas être inclus dans cette base de code.

### Base de travail

Il n’en reste pas moins qu’il s’agit là d’une base de travail, qu’il convient par nature de compléter.

Aussi (et notamment dans le cadre de projets _responsive_) il sera bien plus intéressant d’utiliser des classes plus sémantiques que celles fournies.

La bibliothèque est un utilitaire qui permettra sans doute de factoriser quelques lignes ; mais chaque projet étant différent il reste à votre charge de créer les modules les plus intéressants pour répondre à vos contraintes.

### Objectifs secondaires

Par ailleurs, quelques expérimentations sont mises en place. Elles ne seront peut-être pas utilisées tous les jours (ou même adoptées) mais c‘est l’occasion d’essayer.

On trouve ainsi :

 - un module `flexbox` ;
 - une grille verticale basée sur `inline-block` ;
 - une utilisation des échelles typographiques.

Toutes restent bien sûr optionnelles dans l’usage de la bibliothèque.

## Dépendances

[SassyLists](https://github.com/at-import/SassyLists) est nécessaire à certains calculs internes.
La gestion des prefixes navigateurs est reléguée à [Autoprefixer](https://github.com/postcss/autoprefixer), ce qui permet de conserver une écriture CSS plus conforme aux standards et donc plus lisible.

Pour le reste, un minimum de bibliothèques sont chargées par défaut : _normalize_ et _sass-mq_. Néanmoins d’autres sont vivement conseillées et elles sont donc embarquées et pré-configurées. Aussi, elles sont définies dans les outils de gestion de dépendances.

Ce sont des bibliothèques documentées, maintenues et éprouvées. Attention toutefois aux compatibilités entre Compass et Bourbon, qu’il vaudra mieux charger alors module par module.

## Architecture

Tous les fichiers préfixés par un `_` n’ont pas vocation à être compilés directement mais à être inclus. 

L’usage des `!important` est fortement limité. Seules quelques classes suffixées par `-i` en font usage.

Toutes les variables, tous les `mixins`, toutes les `functions`, toutes les classes sont préfixées par `smash-` pour minimiser les risques de conflits avec d’autres bibliothèques.

Côté _framework_, on trouve :

- `core` contient les _mixins_, les _functions_ et les composants nécessaires au bon fonctionnement de la bibliothèque ;
- `utils` contient les classes utilitaires de la bibliothèque.

Côté projet, on trouve :

- `components` regroupe les styles par composants ;
- `layouts` regroupe les styles via une découpe simpliste de la page pour y décrire les agencements principaux ;
- `pages` liste des styles génériques à certaines pages ;
- `plugins` liste les _plugins_ éventuellement utilisés ;
- `themes` regroupe les variantes graphiques éventuelles.
 
## Documentation

La documentation générée est disponible dans le dossier `docs` et tout les fichiers sont également commentés.

## Démonstrations

La documentation embarque quelques démonstrations, accessibles via le menu principal en haut de page.
 

